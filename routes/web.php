<?php


//website routes
Route::get('/', 'RideController@index');
Route::get('/register', 'StudentController@showRegisterForm')->name('user.create');
Route::post('/register', 'StudentController@store')->name('user.store');
Route::get('/show-login', 'StudentController@showLoginForm')->name('user.loginForm');
Route::get('my-booking', 'RideController@myBooking')->name('booking.me')->middleware('auth');
Route::delete('my-booking/{booking}', 'RideController@deleteMyBooking')->name('deleteMyBooking')->middleware('auth');

Route::view('/admin/login', 'admin.login');
Route::post('/admin/login', 'Admin\AdminLoginController@login')->name('admin.login.submit');
Route::post('/admin/logout', 'Admin\AdminLoginController@logout')->name('admin.logout');


Route::get('/home', function () {
    return redirect()->to('/');
});

Route::get('rides/{ride}', 'RideController@book')->name('rides.book')->middleware('auth');
Route::resource('rides', 'RideController')->only(['index']);
Auth::routes(['register' => false]);

// Admin
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth:admin']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('users', 'UsersController');
    Route::resource('buses', 'BusesController');
    Route::resource('rides', 'RidesController');
    Route::resource('bookings', 'BookingsController');
});

Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth:admin']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
