<?php

use App\Bus;
use Faker\Factory;
use Illuminate\Database\Seeder;

class RidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $buses = Bus::pluck('id');
        $rides = [];
        $places1 = [
            'حي العزيزية',
            'حي الملك فهد',
            'حي الربوة',
            'حي سيد الشهداء',
            'حي قباء',
            'حي العنابس',
            'حي السحمان',
            ' حي المستراح',
            'حي السيح',
            'حي الوبرة',
            'حي عروة',
            'حي الدخل المحدود',
            'حي العصبة',
            'حي شوران',
            'حي الراية',
            'حي الفتح',
            'حي البحر',
            'حي الجبور',
            'حي النصر',
            'حي العنبرية',
            'حي العوالي',
        ];

        $places2 = [
            ' حي العيون',
            'حي المناخة',
            'حي الأغوات',
            'حي الساحة',
            'حي زقاق الطيار',
            'حي الحرة الشرقية',
            'حي التاجوري',
            'حي باب المجيدي',
            'حي باب الشامي',
            'حي الحرة الغربية',
            'حي الجرف',
            'حي الدويمة',
            'حي القبلتين',
            'حي أبيار علي',
            'حي الخالدية',
            ' حي الاسكان',
            'حي المطار',
            'حي البيداء',
            'حي تلعة الهبوب',
            'حي المبعوث',
        ];


        foreach (range(1, 50) as $id) {
            $departureTime = now()
                ->setTime(mt_rand(6, 18), collect([0, 15, 30, 45])->random(), 0)
                ->addDays(mt_rand(1, 4));

            $rides[] = [
                'bus_id' => $buses->random(),
                'departure_place' => $places1[rand(0,15)],
                'arrival_place' => $places2[rand(0,15)],
                'departure_time' => $departureTime,
                'arrival_time' => $departureTime->clone()->addHours(1, 6),
                'is_booking_open' => true,
            ];

            $faker->unique(true);
        }

        \App\Ride::insert($rides);
    }
}
