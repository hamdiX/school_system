<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidesTable extends Migration
{
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->id('id');
            $table->string('departure_place');
            $table->string('arrival_place');
            $table->datetime('departure_time');
            $table->datetime('arrival_time');
            $table->boolean('is_booking_open')->default(0);
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->string('driver_phone')->nullable();
            $table->foreign('admin_id')->references('admin_id')->on('admins');
//            $table->timestamps();
//            $table->softDeletes();
        });
    }
}
