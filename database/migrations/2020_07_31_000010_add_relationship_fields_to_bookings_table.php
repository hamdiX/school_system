<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToBookingsTable extends Migration
{
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('ride_id');
            $table->unsignedBigInteger('student_id');
            $table->foreign('ride_id')->references('id')->on('rides');
            $table->foreign('student_id')->references('id')->on('students');
        });
    }
}
