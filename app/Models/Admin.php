<?php

namespace App\Models;

use App\Booking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class Admin extends  Authenticatable
{
    use  Notifiable;

    public $table = 'admins';
    public $timestamps = false;
    protected $primaryKey = 'admin_id';



    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
    ];


    public function setPasswordAttribute($input)
    {


        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }
}
