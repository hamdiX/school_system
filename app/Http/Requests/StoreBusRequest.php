<?php

namespace App\Http\Requests;

use App\Bus;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreBusRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => [
                [
                    'string',
                    'required',
                ],
            ],
            'places_available' => [
                [
                    'required',
                    'integer',
                    'min:-2147483648',
                    'max:2147483647',
                ],
            ],
            'image' => [
                [
                    'required',
                    'image',
                ],
            ],
        ];
    }
}
