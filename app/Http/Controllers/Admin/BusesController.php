<?php

namespace App\Http\Controllers\Admin;

use App\Bus;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBusRequest;
use App\Http\Requests\StoreBusRequest;
use App\Http\Requests\UpdateBusRequest;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BusesController extends Controller
{
    use UploadFile;

    public function index()
    {
        $buses = Bus::all();
        return view('admin.buses.index', compact('buses'));
    }

    public function create()
    {
        return view('admin.buses.create');
    }

    public function store(StoreBusRequest $request)
    {
        $data = $request->all();
        $data['image'] = $this->upload($request->image);
        $bus = Bus::create($data);
        session()->flash('message', 'تم عملية الإضافة بنجــــــاح');
        return redirect()->route('admin.buses.index');
    }

    public function edit(Bus $bus)
    {
        return view('admin.buses.edit', compact('bus'));
    }

    public function update(UpdateBusRequest $request, Bus $bus)
    {
        $data = $request->all();
        if ($request->has('image')) {
            $data['image'] = $this->upload($request->image);
        }
        $bus->update($data);
        session()->flash('message', 'تم عملية التعديل بنجــــــاح');
        return redirect()->route('admin.buses.index');
    }

    public function show(Bus $bus)
    {
        return view('admin.buses.show', compact('bus'));
    }

    public function destroy(Bus $bus)
    {
        $bus->delete();
        session()->flash('message', 'تم عملية الحذف بنجــــــاح');
        return back();
    }

    public function massDestroy(MassDestroyBusRequest $request)
    {
        Bus::whereIn('id', request('ids'))->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
