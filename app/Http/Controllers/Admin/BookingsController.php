<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyBookingRequest;
use App\Http\Requests\StoreBookingRequest;
use App\Http\Requests\UpdateBookingRequest;
use App\Ride;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class BookingsController extends Controller
{
    public function index()
    {
        $bookings = Booking::all();
        $rides = Ride::get();
        $selectedRide = null;
        if (request()->input('ride_id')) {
            $selectedRide = optional($rides->find(request()->input('ride_id')))->route;
        }
        return view('admin.bookings.index', compact('bookings', 'rides', 'selectedRide'));
    }

    public function create()
    {
        $rides = Ride::all()->pluck('route', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('admin.bookings.create', compact('rides'));
    }

    public function store(StoreBookingRequest $request)
    {
        $booking = Booking::create($request->all());
        return redirect()->route('admin.bookings.index');
    }

    public function edit(Booking $booking)
    {
        $rides = Ride::all()->pluck('route', 'id')->prepend(trans('global.pleaseSelect'), '');
        $booking->load('ride');
        return view('admin.bookings.edit', compact('rides', 'booking'));
    }

    public function update(UpdateBookingRequest $request, Booking $booking): RedirectResponse
    {
        $booking->update($request->all());
        return redirect()->route('admin.bookings.index');
    }

    public function show(Booking $booking)
    {
        $booking->load('ride');
        return view('admin.bookings.show', compact('booking'));
    }

    public function destroy(Booking $booking)
    {
        $booking->delete();
        return back();
    }
}
