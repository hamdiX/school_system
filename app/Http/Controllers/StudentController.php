<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreUserRequest;

class StudentController extends Controller
{
    public function showRegisterForm()
    {
        return view('website.register');
    }

    public function store(StoreUserRequest $request): RedirectResponse
    {
        $user = User::create($request->all());
        session()->flash('success', 'تم انشاء حساب جديد بنجاح .. يمكنك تسجيل الدخول الأن ');
        return redirect()->route('user.loginForm');
    }

    public function showLoginForm()
    {
        return view('website.login');
    }
}
