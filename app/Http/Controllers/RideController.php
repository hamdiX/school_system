<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Http\Requests\BookRideRequest;
use App\Ride;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RideController extends Controller
{
    public function index(Request $request)
    {
        $ridesDates = Ride::with('bus')
            ->where('is_booking_open', 1)
            ->orderBy('departure_time', 'asc')
            ->get()
            ->groupBy(function ($ride) {
                return Carbon::parse($ride->departure_time)->format('Y-m-d');
            });
        if($request->filled('ride_id_1') && $request->filled('ride_id_2')) {
            $ridesDates = Ride::with('bus')
                ->where('departure_place','like', '%' . $request->ride_id_1. '%')
                ->where('arrival_place','like', '%' . $request->ride_id_2. '%')
                ->where('is_booking_open', 1)
                ->orderBy('departure_time', 'asc')
                ->get()
                ->groupBy(function ($ride) {
                    return Carbon::parse($ride->departure_time)->format('Y-m-d');
                });
        }

        $rides = Ride::all();
        return view('website.home', compact('ridesDates', 'rides'));
    }

    public function book(Ride $ride): RedirectResponse
    {
        $data['student_id'] = Auth::id();
        $data['ride_id'] = $ride->id;
        $data = array_merge($data, ['status' => 'processing']);
        $ride = Ride::with('bus')
            ->withCount('confirmedBookings as bookings_count')
            ->find($ride->id);
        if (
            !optional($ride)->bus ||
            !$ride->is_booking_open ||
            $ride->bus->places_available <= $ride->bookings_count ||
            now()->lessThanOrEqualTo($ride->depart_time)
        ) {
            session()->flash('error', 'هذه الرحلة لم تعد متوفرة');
            return redirect()->back()->with('alert', 'هذه الرحلة لم تعد متوفرة');
        }
        Booking::create($data);
        session()->flash('success', 'تم حجز الباص بنجاح وهي قيد المعالجة حاليًا');
        return redirect()->back();
    }

    public function myBooking()
    {
        $bookings = auth()->user()->booking;
        return view('website.booking', compact('bookings'));
    }

    public function deleteMyBooking(Booking $booking): RedirectResponse
    {
        if ($booking->status === 'confirmed') {
            session()->flash('error', 'لقد تم تأكيد الحجز من قبل أداره الموقع ولا يمكن الغــاء الحجز الأن');
            return back();
        }
        $booking->delete();
        session()->flash('success', 'تم إلغاء الحــجز بنجـــاح');
        return back();
    }
}
