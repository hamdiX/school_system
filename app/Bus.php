<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Bus extends Model
{
    public $table = 'buses';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = false;
    protected $fillable = [
        'name',
        'places_available',
        'image',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'image_path'
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getSelectNameAttribute()
    {
        return $this->name . ' (' . $this->places_available . ' ' . \Str::plural('place', $this->places_available) . ')';
    }

    public function getImagePathAttribute(): string
    {
        if ($this->image && file_exists(public_path('uploads') . '/' . $this->image)) {
            return asset('uploads') . '/' . $this->image;
        }

        return asset('img/bus.png');
    }
}
