<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/app-style-switcher.js')}}"></script>
<script src="{{asset('js/waves.js')}}"></script>
<script src="{{asset('js/sidebarmenu.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/general.js')}}"></script>
<script src="{{asset('js/swal.js')}}"></script>
<script src="{{asset('js/jquery-datatables.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendor/jsvalidation/js/jsvalidation.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    function confirmWithSwal(message, target = null, submitForm = false) {
        Swal.fire({
            title: message,
            showCancelButton: true,
            confirmButtonText: 'Yes',
        }).then((result) => {
            if (submitForm) {
                if (result.isConfirmed && target) {
                    document.getElementById(target).submit()
                }
            }
        })
    }

    $('#example').DataTable();
    $('.datatable').DataTable();

    @if(session('success'))
    Swal.fire('Success', '{{session('success')}}', 'success')
    @endif

    @if(session('error'))
    Swal.fire('Error !', '{{session('error')}}', 'error')
    @endif
    $('.js-example-basic-single').select2();
</script>
