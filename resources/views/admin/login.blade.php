<!DOCTYPE html>
<html lang="ar" direction="rtl" dir="rtl" style="direction: rtl">
<head>
    <meta charset="utf-8">
    <title>احجـــــزلــى</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    @include('website.layouts.head')
    <style>
        .nav-bar {
            margin-top: 0 !important;
        }
        .box2 {
            box-shadow: 0px 1px 4px 1px #020202d6;
            border-radius: 18px;
        }
    </style>
</head>
<body>
    <div class="container-xxl py-5 w-50">
        <form action="{{ route('admin.login.submit') }}" method="post" id="formLogin">
            <div class="row g-2 p-2 box2 ">
                <h4 class="text-center">تسجـــيل الــــدخول</h4>
                @csrf
                <div class="mb-3">
                    <label for="email" class="form-label"> البريد الألكترونى<span class="text-danger">*</span></label>
                    <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{old('email')}}" id="email" placeholder=" البريد الألكترونى">
                    @error('email')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label  @error('password') is-invalid @enderror">   كلمة المرور<span class="text-danger">*</span></label>
                    <input type="password" autocomplete="new-password" class="form-control" name="password" id="password" placeholder="كلمة المرور">
                    @error('password')
                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button type="submit" form="formLogin" class="btn btn-dark border-0 w-100 py-3"><i class="fa fa-save"></i> تسجيل الدخول</button>
                </div>
                <div class="col-6 text-right">

                </div>
            </div>
        </form>
    </div>
</body>
</html>
