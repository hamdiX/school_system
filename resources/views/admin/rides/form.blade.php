<div class="form-group col-md-6">
    <label class="required" for="bus_id">{{ trans('cruds.ride.fields.bus') }}</label>
    <select class="form-control select2 {{ $errors->has('bus') ? 'is-invalid' : '' }}" name="bus_id" id="bus_id" required>
        @foreach($buses as $id => $bus)
            <option value="{{ $id }}" {{ isset($ride) && ($ride->bus ? $ride->bus->id : old('bus_id')) == $id ? 'selected' : '' }}>{{ $bus }}</option>
        @endforeach
    </select>
    @if($errors->has('bus'))
        <div class="invalid-feedback">
            {{ $errors->first('bus') }}
        </div>
    @endif
</div>
<div class="form-group col-md-6">
    <label class="required" for="departure_place">{{ trans('cruds.ride.fields.departure_place') }}</label>
    <input class="form-control {{ $errors->has('departure_place') ? 'is-invalid' : '' }}" type="text" name="departure_place" id="departure_place"
           value="{{ isset($ride) ? $ride->departure_place  : old('departure_place') }}" required>
    @if($errors->has('departure_place'))
        <div class="invalid-feedback">
            {{ $errors->first('departure_place') }}
        </div>
    @endif
</div>
<div class="form-group col-md-6">
    <label class="required" for="arrival_place">{{ trans('cruds.ride.fields.arrival_place') }}</label>
    <input class="form-control {{ $errors->has('arrival_place') ? 'is-invalid' : '' }}" type="text" name="arrival_place" id="arrival_place"
           value="{{ isset($ride) ? $ride->departure_place  : old('departure_place') }}" required>
    @if($errors->has('arrival_place'))
        <div class="invalid-feedback">
            {{ $errors->first('arrival_place') }}
        </div>
    @endif
</div>
<div class="form-group col-md-6">
    <label class="required" for="departure_time">{{ trans('cruds.ride.fields.departure_time') }}</label>
    <input class="form-control datetime {{ $errors->has('departure_time') ? 'is-invalid' : '' }}" type="text" name="departure_time"
           id="departure_time"  value="{{ isset($ride) ? $ride->departure_time  : old('departure_time') }}" required>
    @if($errors->has('departure_time'))
        <div class="invalid-feedback">
            {{ $errors->first('departure_time') }}
        </div>
    @endif
</div>
<div class="form-group col-md-6">
    <label class="required" for="arrival_time">{{ trans('cruds.ride.fields.arrival_time') }}</label>
    <input class="form-control datetime {{ $errors->has('arrival_time') ? 'is-invalid' : '' }}" type="text" name="arrival_time"
           id="arrival_time"  value="{{ isset($ride) ? $ride->arrival_time  : old('arrival_time') }}" required>
    @if($errors->has('arrival_time'))
        <div class="invalid-feedback">
            {{ $errors->first('arrival_time') }}
        </div>
    @endif
</div>

<div class="form-group col-md-6">
    <label class="required" for="arrival_time">رقم السائق</label>
    <input class="form-control {{ $errors->has('driver_phone') ? 'is-invalid' : '' }}" type="text" name="driver_phone"
             value="{{ isset($ride) ? $ride->driver_phone  : old('driver_phone') }}" required>
    @if($errors->has('driver_phone'))
        <div class="invalid-feedback">
            {{ $errors->first('driver_phone') }}
        </div>
    @endif
</div>

<div class="form-group col-md-6">
    <div class="form-check {{ $errors->has('is_booking_open') ? 'is-invalid' : '' }}">
        <input class="form-check-input" type="checkbox" name="is_booking_open" id="is_booking_open" value="1"
            {{( isset($ride) && $ride->is_booking_open == 1) || (old('is_booking_open', 0) == 1) || (old('is_booking_open') === null) ? 'checked' : '' }}>
        <label class="form-check-label" for="is_booking_open">{{ trans('cruds.ride.fields.is_booking_open') }}</label>
    </div>
    @if($errors->has('is_booking_open'))
        <div class="invalid-feedback">
            {{ $errors->first('is_booking_open') }}
        </div>
    @endif
</div>
<div class="form-group col-md-6">
    <button class="btn btn-danger" type="submit">
        {{ trans('global.save') }}
    </button>
</div>
