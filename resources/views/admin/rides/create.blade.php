@extends('admin.layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.ride.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.rides.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
              @include('admin.rides.form')
            </div>
        </form>
    </div>
</div>



@endsection
