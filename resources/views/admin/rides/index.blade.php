@extends('admin.layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <a class="btn btn-outline-primary" href="{{ route('admin.rides.create') }}">
            <i class="fa fa-plus"></i>
            إضافة
        </a>

        عرض قائمة خطوط الباصات
    </div>

    <div class="card-body">
        <div class="">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>{{ trans('cruds.ride.fields.bus') }}</th>
                        <th>الطريق</th>
                        <th>وقت الإقلاع</th>
                        <th>وقت الوصول</th>
                        <th>هل متاح ؟ </th>
                        <th>المقاعد المتاحة </th>
                        <th>الحجوزات المؤكدة</th>
                        <th>الحجوزات المرفوضة</th>
                        <th>معالجة الحجوزات</th>
                        <th>العمليات</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rides as $key => $ride)
                        <tr data-entry-id="{{ $ride->id }}">
                            <td>{{$ride->id }}</td>
                            <td>{{ $ride->bus->select_name ?? '' }}</td>
                            <td>{{ $ride->route ?? '' }}</td>
                            <td>{{ Carbon\Carbon::parse($ride->departure_time)->format('m-d H:i') ?? '' }}</td>
                            <td>{{ Carbon\Carbon::parse($ride->arrival_time)->format('m-d H:i') ?? '' }}</td>
                            <td>
                                @if( $ride->is_booking_open)
                                    <i class="fa fa-check-circle text-success"></i>
                                @else
                                    <i class="fa fa-times-circle text-danger"></i>
                                @endif
                            </td>
                            <td>{{ optional($ride->bus)->places_available - $ride->confirmed_bookings_count }}</td>
                            <td>
                                <a href="{{ route('admin.bookings.index', ['ride_id' => $ride->id, 'status' => 'confirmed']) }}">
                                    {{ $ride->confirmed_bookings_count }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('admin.bookings.index', ['ride_id' => $ride->id, 'status' => 'rejected']) }}">
                                    {{ $ride->rejected_bookings_count }}
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('admin.bookings.index', ['ride_id' => $ride->id, 'status' => 'processing']) }}">
                                    {{ $ride->processing_bookings_count }}
                                </a>
                            </td>
                            <td>
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.rides.show', $ride->id) }}">
                                        {{ trans('global.view') }}
                                    </a>

                                    <a class="btn btn-xs btn-info" href="{{ route('admin.rides.edit', $ride->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                    <form action="{{ route('admin.rides.destroy', $ride->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
