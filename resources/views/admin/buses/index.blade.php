@extends('admin.layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <a class="btn btn-outline-primary" href="{{ route('admin.buses.create') }}">
            <i class="fa fa-plus"></i>
            إضافة
        </a>

        عرض قائمة الباصات
    </div>

    <div class="card-body">
        <div class="">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>رقم الباص</th>
                        <th>{{ trans('cruds.bus.fields.name') }}</th>
                        <th>{{ trans('cruds.bus.fields.places_available') }}</th>
                        <th>صــورة الباص</th>
                        <th>&nbsp;العمليات</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($buses as $key => $bus)
                        <tr data-entry-id="{{ $bus->id }}">
                            <td> {{$key + 1}}</td>
                            <td>{{ $bus->id ?? '' }}</td>
                            <td>{{ $bus->name ?? '' }}</td>
                            <td>{{ $bus->places_available ?? '' }}</td>
                            <td><img src="{{$bus->image_path}}" class="img-thumbnail" height="100px" width="100px"></td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{ route('admin.buses.show', $bus->id) }}">
                                    {{ trans('global.view') }}
                                </a>
                                <a class="btn btn-xs btn-info" href="{{ route('admin.buses.edit', $bus->id) }}">
                                    {{ trans('global.edit') }}
                                </a>
                                <form action="{{ route('admin.buses.destroy', $bus->id) }}" method="POST"
                                      onsubmit="return confirm('{{ trans('global.areYouSure') }}');"
                                      style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

