@extends('admin.layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        عرض قائمة الحجوزات
    </div>

    <div class="card-body">
        <div class="">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>{{ trans('cruds.booking.fields.ride') }}</th>
                        <th>{{ trans('cruds.booking.fields.name') }}</th>
                        <th>{{ trans('cruds.booking.fields.email') }}</th>
                        <th>العنوان</th>
                        <th>{{ trans('cruds.booking.fields.phone') }}</th>
                        <th>{{ trans('cruds.booking.fields.status') }}</th>
                        <th>العمليات</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach($bookings as $key => $booking)
                        <tr data-entry-id="{{ $booking->id }}">
                            <td>{{ $booking->id }}</td>
                            <td>{{ $booking->ride->route}}</td>
                            <td>{{ optional($booking->user)->name }}</td>
                            <td>{{ optional($booking->user)->email }}</td>
                            <td>{{ optional($booking->user)->address  }}</td>
                            <td>{{ optional($booking->user)->phone  }}</td>
                            <td>{{ App\Booking::STATUS_SELECT[$booking->status] }}</td>
                            <td>
                                <a class="btn btn-xs btn-primary"
                                   href="{{ route('admin.bookings.show', $booking->id) }}">
                                    {{ trans('global.view') }}
                                </a>
                                <a class="btn btn-xs btn-info" href="{{ route('admin.bookings.edit', $booking->id) }}">
                                    {{ trans('global.edit') }}
                                </a>
                                <form action="{{ route('admin.bookings.destroy', $booking->id) }}" method="POST"
                                      onsubmit="return confirm('{{ trans('global.areYouSure') }}');"
                                      style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger"
                                           value="{{ trans('global.delete') }}">
                                </form>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

