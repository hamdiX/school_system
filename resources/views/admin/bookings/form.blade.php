<div class="row">
    <div class="form-group col-md-4">
        <label class="required" for="ride_id">{{ trans('cruds.booking.fields.ride') }}</label>
        <select class="form-control select2 {{ $errors->has('ride') ? 'is-invalid' : '' }}" name="ride_id" id="ride_id" required>
            @foreach($rides as $id => $ride)
{{--                <option value="{{ $id }}" {{ old('ride_id') == $id ? 'selected' : '' }}>{{ $ride }}</option>--}}
                <option value="{{ $id }}" {{ (isset($booking) && $booking->ride ? $booking->ride->id : old('ride_id')) == $id ? 'selected' : '' }}>{{ $ride }}</option>
            @endforeach
        </select>
        @if($errors->has('ride'))
            <div class="invalid-feedback">
                {{ $errors->first('ride') }}
            </div>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label class="required" for="name">{{ trans('cruds.booking.fields.name') }}</label>
        <input readonly class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ isset($booking) ? optional($booking->user)->name : old('name', '') }}" required>
        @if($errors->has('name'))
            <div class="invalid-feedback">
                {{ $errors->first('name') }}
            </div>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label class="required" for="email">{{ trans('cruds.booking.fields.email') }}</label>
        <input readonly class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{  isset($booking) ? optional($booking->user)->email :  old('email') }}" required>
        @if($errors->has('email'))
            <div class="invalid-feedback">
                {{ $errors->first('email') }}
            </div>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label class="required" for="phone">{{ trans('cruds.booking.fields.phone') }}</label>
        <input readonly class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" type="text" name="phone" id="phone" value="{{ isset($booking) ? optional($booking->user)->phone :  old('phone', '') }}" required>
        @if($errors->has('phone'))
            <div class="invalid-feedback">
                {{ $errors->first('phone') }}
            </div>
        @endif
    </div>
    <div class="form-group col-md-4">
        <label class="required">{{ trans('cruds.booking.fields.status') }}</label>
        <select  class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
            <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
            @foreach(App\Booking::STATUS_SELECT as $key => $label)
                <option value="{{ $key }}" {{ isset($booking) && old('status', $booking->status) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
            @endforeach
        </select>
        @if($errors->has('status'))
            <div class="invalid-feedback">
                {{ $errors->first('status') }}
            </div>
        @endif
    </div>
    <div class="form-group col-md-6">
        <button class="btn btn-danger" type="submit">
            {{ trans('global.save') }}
        </button>
    </div>
</div>
