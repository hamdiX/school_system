<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            احجـــزلى
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>

        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-user-alt">

                </i>
                {{ trans('cruds.userManagement.title') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.buses.index") }}"
               class="c-sidebar-nav-link {{ request()->is('admin/buses') || request()->is('admin/buses/*') ? 'active' : '' }}">
                <i class="fa-fw fas fa-bus c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.bus.title') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.rides.index") }}"
               class="c-sidebar-nav-link {{ request()->is('admin/rides') || request()->is('admin/rides/*') ? 'active' : '' }}">
                <i class="fa-fw fas fa-map-marked c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.ride.title') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.bookings.index") }}"
               class="c-sidebar-nav-link {{ request()->is('admin/bookings') || request()->is('admin/bookings/*') ? 'active' : '' }}">
                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                </i>
                {{ trans('cruds.booking.title') }}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}"
               href="{{ route('profile.password.edit') }}">
                <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                </i>
                تغير كلمة المرور
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link"
               onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>

        <li class="c-sidebar-nav-item">
            <a href="{{url('/')}}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-globe"></i>
                زيارة الموقع
            </a>
        </li>
    </ul>

</div>
