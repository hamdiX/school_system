@extends('website.layouts.app')
@push('css')
    <style>
        .nav-bar {
            margin-top: 0 !important;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl py-5">
       <div class="row">
           <div class="col-md-2"></div>
           <div class="col-md-12">
               <div class="card  text-center" id="busEle">
                   <div class="card-header">
                       عرض قائمة الحجوزات
                   </div>

                   <div class="card-body">
                       <div class="">
                           <table class="table table-bordered">
                               <thead>
                               <tr>
                                   <th width="10">#</th>
                                   <th>{{ trans('cruds.booking.fields.ride') }}</th>
                                   <th>رقم الباص</th>
                                   <th> وقت الأقلاع </th>
                                   <th> وقت الوصول </th>
                                   <th> رقم  السائق </th>
                                   <th>حالة الحجز</th>
                                   <th id="hideThis2">العمليات</th>
                               </tr>

                               </thead>
                               <tbody>
                               @foreach($bookings as $key => $booking)
                                   <tr data-entry-id="{{ $booking->id }}">
                                       <td>{{ $booking->id }}</td>
                                       <td>{{ optional($booking->ride)->route}}</td>
                                       <td>{{ optional($booking->ride)->bus_id}}</td>
                                       <td>{{ optional($booking->ride)->departure_time}}</td>
                                       <td>{{ optional($booking->ride)->arrival_time}}</td>
                                       <td>{{ optional($booking->ride)->driver_phone}}</td>
                                       <td>{{ App\Booking::STATUS_SELECT[$booking->status] }}</td>
                                       <td id="hideThis">
                                           <a onclick="PrintElem('busEle')" style="font-size: 10px" class="btn btn-success p-1 rounded-1" href="#"><i class="fa fa-print"></i>  طبـــاعة</a>

                                           <form action="{{ route('deleteMyBooking', $booking->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                               <input type="hidden" name="_method" value="DELETE">
                                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                               <input style="font-size: 10px" type="submit" class="btn btn-danger p-1 rounded-1" value="إلغــاء ">
                                           </form>
                                       </td>

                                   </tr>
                               @endforeach
                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>
    </div>
@endsection

@push('js')
    <script>
        function PrintElem(elem)
        {
            document.getElementById('hideThis2').style.display = 'none';
            document.getElementById('hideThis').style.display = 'none';
            var mywindow = window.open('', 'PRINT', 'height=600,width=600');
            mywindow.document.write('<html><head><title>' + document.title  + '</title>');
            mywindow.document.write('</head><body >');
            mywindow.document.write('<h1>' + document.title  + '</h1>');
            mywindow.document.write(document.getElementById(elem).innerHTML);
            mywindow.document.write('</body></html>');
            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/
            mywindow.print();
            return true;
        }
    </script>
@endpush
