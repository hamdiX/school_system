@if(session('success') || session('status'))
    <div class="row mb-2 text-center">
        <div class="col-md-3"></div>
        <div class="col-lg-6">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') ?? session('status') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
@endif

@if(session('error'))
    <div class="row mb-2 text-center">
        <div class="col-md-3"></div>
        <div class="col-lg-6">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
@endif
