<!DOCTYPE html>
<html lang="ar" direction="rtl" dir="rtl" style="direction: rtl">
<head>
    <meta charset="utf-8">
    <title>احجـــــزلــى</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    @include('website.layouts.head')
</head>

<body>
<div class="container-xxl bg-white p-0">
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- Spinner End -->

    <!-- Navbar Start -->
    @include('website.layouts.navbar')
    <!-- Navbar End -->

    <!-- Header Start -->
       @yield('header')
    <!-- Header End -->

    <!-- Search Start -->
       @yield('search')
    <!-- Search End -->

      @yield('content')

    <!-- Footer Start -->
    @include('website.layouts.footer')
    <!-- Footer End -->
</div>
@include('website.layouts.foot')
</body>
</html>
