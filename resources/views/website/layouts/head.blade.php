<!-- Favicon -->
<link href="{{asset('website/img/favicon.ico')}}" rel="icon">
<!-- Google Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
<!-- Icon Font Stylesheet -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
<!-- Libraries Stylesheet -->
<link href="{{asset('website/lib/animate/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('website/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
<!-- Customized Bootstrap Stylesheet -->
<link href="{{asset('website/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Template Stylesheet -->
<link href="{{asset('website/css/style-rtl.css')}}" rel="stylesheet">
<style>
    body {
        font-family: 'Cairo', serif !important;
    }
    .box-border {
        border-bottom: 1px solid #fabf39;
        margin-bottom: 24px;
    }
    .box2-border {
        border-top: 1px solid #ffd799;
        border-bottom: 1px solid #ffd799;
        padding: 14px;
    }
</style>
@stack('css')
