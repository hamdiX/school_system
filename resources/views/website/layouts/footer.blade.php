<div class="container-fluid bg-dark text-white-50 footer">
    <div class="container pt-3">
        <div class="row g-5">
            <div class="col-lg-12 col-md-6">
                <h5 class="text-white mb-4">تواصل معنا</h5>
                <p class="mb-2">
                     <span><i class="fa fa-map-marker-alt me-3"></i>المدينــة المنورة</span>
                    <span><i class="fa fa-phone-alt me-3"></i>+966 594430030</span>
                    <span><i class="fa fa-envelope me-3"></i>school@school.com</span>
                </p>
                <div class="d-flex pt-3 pb-3">
                    <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-youtube"></i></a>
                    <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copyright">
            <div class="row">
                <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                    &copy; <a class="border-bottom" href="{{url('/')}}">احجـــزلــى</a>, جميع الحقوق محفوظة.<a class="border-bottom" href="https://htmlcodex.com"></a>
                </div>
                <div class="col-md-6 text-center text-md-end">
                    <div class="footer-menu">
                        <a href="{{url('/')}}">الصفحة الرئيسية</a>
                        <a href="">تواصل معنا</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
