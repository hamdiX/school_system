<div   class="container-fluid bg-primary mb-5" style="padding: 35px;">
    <div class="container" id="searchArea">
       <form method="get" action="{{url('/')}}">
           @csrf
           <div class="row g-2">
               <div class="col-md-6">
                   <div class="row g-2">
                       <div class="col-md-6">
                           <select class="form-select border-0 py-3" name="ride_id_1">
                               <option selected value="">اختر</option>
                               @foreach($rides as $ride)
                                   <option value="{{$ride->departure_place}}">{{$ride->departure_place}}</option>
                               @endforeach
                           </select>
                       </div>
                       <div class="col-md-6">
                           <select class="form-select border-0 py-3" name="ride_id_2">
                               <option selected value="">اختر</option>
                               @foreach($rides as $ride)
                                   <option value="{{$ride->arrival_place}}">{{$ride->arrival_place}}</option>
                               @endforeach
                           </select>
                       </div>
                   </div>
               </div>
               <div class="col-md-4">
                   <button type="submit" class="btn btn-dark border-0 w-100 py-3"><i class="fa fa-search"></i>  </button>
               </div>
               <div class="col-md-2">
                   <a href="{{url('/')}}" class="btn btn-dark border-0 w-100 py-3"><i class="fa fa-trash"></i>  </a>
               </div>
           </div>
       </form>
    </div>
</div>
