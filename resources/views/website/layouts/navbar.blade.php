<div class="container-fluid nav-bar bg-transparent">
    <nav class="navbar navbar-expand-lg bg-white navbar-light py-0 px-4">
        <a href="javascript:void(0);" class="navbar-brand d-flex align-items-center text-center">
            <div class="logo-holder logo-9">
                <a href="">
                    <span><i class="fas fa-bus" aria-hidden="true"></i></span>
                    <h3>احجـــــــزلى</h3>
                </a>
            </div>
        </a>
        <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto">
                <a href="{{url('/')}}" class="nav-item nav-link active">الصفحة الرئيسية</a>
                <a href="{{url('/')}}#searchArea" class="nav-item nav-link">احجـــز الآن</a>
            </div>
            @auth
                @if(auth()->user()->is_admin == 1)
                    <a href="{{url('/admin')}}" class="btn btn-primary px-3 d-none d-lg-flex"><i style="margin-left: 10px" class="fas fa-tachometer-alt mt-1 pl-3"></i>  إدارة الموقع </a>
                @else
                <a href="{{route('booking.me')}}" class="btn btn-primary px-3 d-none d-lg-flex  mx-3"><i style="margin-left: 10px" class="fas fa-user mt-1 pl-3"></i> حجــوزاتــى </a>
                @endif
                <a onclick="event.preventDefault(); document.getElementById('logoutform').submit();" href="#" class="btn btn-primary px-3 d-none d-lg-flex mx-3"><i style="margin-left: 10px" class="fas fa-sign-out-alt mt-1 pl-3"></i>   تسجــيل الخروج </a>
            @else
                <a href="{{url('show-login')}}" class="btn btn-primary px-3 d-none d-lg-flex mx-3"><i style="margin-left: 10px" class="fas fa-sign-out-alt mt-1 pl-3"></i> تسجــيل الدخول  </a>
                <a href="{{route('user.create')}}" class="btn btn-primary px-3 d-none d-lg-flex"><i style="margin-left: 10px" class="fas fa-plus mt-1 pl-3"></i> حســاب جديد </a>
            @endauth

        </div>
    </nav>

    @include('website.layouts.messages')

</div>
<form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
