@extends('website.layouts.app')

@section('header')
    @include('website.layouts.header')
@endsection

@section('search')
    @include('website.layouts.search')
@endsection

@section('content')
    <div class="container-xxl py-5">
        <div class="container">
            @include('website.layouts.messages')
            @forelse ($ridesDates as $date => $rides)
                <div class="row g-0 gx-5 align-items-end">
                    <div class="col-lg-12 text-center">
                        <div class="text-start mx-auto mb-5">
                            <h4 class="text-center box2-border" class="mb-3 mt-3">{{$date}} <i class="fas fa-calendar-alt"></i></h4>
                        </div>
                    </div>
                </div>
                    <div class="row g-4">
                        @foreach ($rides as $ride)
                        <div class="col-lg-3 col-md-6">
                            <div class="property-item rounded">
                                <div class="position-relative" style=" padding:34px;">
                                    <a href=""><img class="img-fluid" src="{{optional($ride->bus)->image_path}}" alt=""></a>
                                    <div class="bg-primary rounded text-white position-absolute start-0 top-0 py-1 px-3">
                                        <a class="text-white" href="{{route('rides.book', $ride->id)}}"> احجــز الان <i class="fa fa-cart-plus"></i></a>
                                    </div>
                                    <div class="bg-white rounded-top text-primary position-absolute start-0 bottom-0 mx-4 pt-1 px-3">
                                    </div>
                                </div>
                                <div class="p-3 pb-0">
                                    <p class="text-danger mb-3">
                                        <span> {{ Carbon\Carbon::parse($ride->departure_time)->format('g:i A') }} <i class="fa fa-clock"></i></span>
                                        <span class="float-start"> {{ Carbon\Carbon::parse($ride->arrival_time)->format('g:i A') }} <i class="fa fa-clock"></i></span>
                                    </p>
                                    <p style="font-size:13px;font-weight:800;"><i class="fa fa-map-marker-alt text-danger pb-4 ml-r"></i>{{ $ride->route }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
            @empty
                <div class="text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 600px;">
                    <h1 class="mb-3">لا توجد باصات متاحة</h1>
                </div>
            @endforelse
        </div>
        </div>
    </div>
@endsection
