@extends('website.layouts.app')
@push('css')
    <style>
        .nav-bar {
            margin-top: 0 !important;
        }
        .box2 {
            box-shadow: 0px 1px 4px 1px #020202d6;
            border-radius: 18px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl py-5 w-50">
        <form action="{{route('user.store')}}" method="post" id="formRegister">
            <div class="row g-2 p-2 box2 ">
                <h4 class="text-center">انشاء حساب جديد</h4>
                   @csrf
                   <div class="col-md-6 mb-3">
                       <label for="name" class="form-label"> الأســم<span class="text-danger">*</span></label>
                       <input type="text" class="form-control  @error('name') is-invalid @enderror" name="name" value="{{old('name')}}" id="name" placeholder="الأســم">
                       @error('name')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                   <div class="col-md-6 mb-3">
                       <label for="email" class="form-label"> البريد الألكترونى <span class="text-danger">*</span></label>
                       <input type="email" autocomplete="new-email" class="form-control  @error('email') is-invalid @enderror" name="email" id="email" placeholder="البريد الألكترونى">
                       @error('email')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                   <div class="col-md-6 mb-3">
                       <label for="password" class="form-label  @error('password') is-invalid @enderror">   كلمة المرور<span class="text-danger">*</span></label>
                       <input type="password" autocomplete="new-password" class="form-control" name="password" id="password" placeholder="كلمة المرور">
                       @error('password')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                   <div class="col-md-6 mb-3">
                       <label for="password_confirmation  @error('password_confirmation') is-invalid @enderror" class="form-label">تأكيد كلمة المرور  <span class="text-danger">*</span></label>
                       <input type="password" autocomplete="new-password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="تأكيد كلمة المرور ">
                       @error('password_confirmation')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                   <div class="col-md-6 mb-3">
                       <label for="address" class="form-label">العنوان</label>
                       <input type="text" class="form-control  @error('address') is-invalid @enderror" name="address" id="address" placeholder="العنوان">
                       @error('address')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                   <div class="col-md-6 mb-3">
                       <label for="phone" class="form-label">رقم الجوال</label>
                       <input type="number" class="form-control  @error('phone') is-invalid @enderror" name="phone" id="phone" placeholder="رقم الجوال">
                       @error('phone')
                       <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                       @enderror
                   </div>

                <div class="mb-3 col-md-2"></div>
                <div class="mb-3 col-md-6">
                    <button type="submit" form="formRegister" class="btn btn-dark border-0 w-50"><i class="fa fa-save"></i></button>
                </div>
                <div class="mb-3 col-md-2">
                    <a class="btn-link text-decoration-underline" href="{{url('show-login')}}">تسجيل الدخول</a>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
